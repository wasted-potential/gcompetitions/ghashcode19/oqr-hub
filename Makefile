CC      = gcc
LD      = gcc
CFLAGS  = -std=c11 -O2 -Wall -Wextra -Wno-unused-parameter -Wno-unused-function -Wno-unused-result -pedantic -g
BIN_DIR = bin
BLD_DIR = build
DOC_DIR = docs
OUT_DIR = out
SRC_DIR = src
SRC     = $(wildcard $(SRC_DIR)/*.c)
OBJS    = $(patsubst $(SRC_DIR)/%.c,$(BLD_DIR)/%.o,$(SRC))
DEPS    = $(patsubst $(BLD_DIR)/%.o,$(BLD_DIR)/%.d,$(OBJS))
PROGRAM = solution

vpath %.c $(SRC_DIR)

.DEFAULT_GOAL = all

.PHONY: run fmt lint leak-check doc checkdirs all clean

$(BLD_DIR)/%.d: %.c
	$(CC) -M $(CFLAGS) $(INCLUDES) $< -o $@

$(BLD_DIR)/%.o: %.c
	$(CC) -c $(CFLAGS) $(INCLUDES) $< -o $@

$(BIN_DIR)/$(PROGRAM): $(DEPS) $(OBJS)
	$(CC) $(CFLAGS) $(INCLUDES) -o $@ $(OBJS)

run: $(BIN_DIR)/$(PROGRAM)
	@./$(BIN_DIR)/$(PROGRAM) data/a_example.in

fmt:
	@echo "C and Headers files:"
	@-clang-format -verbose -style="{BasedOnStyle: Google, IndentWidth: 4}" -i $(SRC_DIR)/*.c $(SRC_DIR)/*.h
	@echo "Shell files:"
	@-shfmt -l -s -w -i 2 .

lint:
	@splint -retvalint -I $(SRC_DIR)/*.c,*.h

leak-check: $(BIN_DIR)/$(PROGRAM)
	@valgrind --vgdb=no --tool=memcheck --leak-check=yes ./$(BIN_DIR)/$(PROGRAM) $(input)

doc:
	@doxygen $(DOC_DIR)/Doxyfile

test:
	@echo "Write some tests!"

checkdirs:
	@mkdir -p $(BLD_DIR)
	@mkdir -p $(BIN_DIR)
	@mkdir -p $(DOC_DIR)

zip: $(BIN_DIR)/$(PROGRAM)
	./$(BIN_DIR)/$(PROGRAM) data/a_example.in           > $(OUT_DIR)/a_example.out
	./$(BIN_DIR)/$(PROGRAM) data/b_lovely_landscapes.in > $(OUT_DIR)/b_lovely_landscapes.out
	./$(BIN_DIR)/$(PROGRAM) data/c_memorable_moments.in > $(OUT_DIR)/c_memorable_moments.out
	./$(BIN_DIR)/$(PROGRAM) data/d_pet_pictures.in      > $(OUT_DIR)/d_pet_pictures.out
	./$(BIN_DIR)/$(PROGRAM) data/e_shiny_selfies.in     > $(OUT_DIR)/e_shiny_selfies.out
	zip -r $(OUT_DIR)/solution.zip $(SRC_DIR)

all: checkdirs $(BIN_DIR)/$(PROGRAM)

clean:
	@echo "Cleaning..."
	@echo ""
	@-cat .art/maid.ascii
	@-rm -rd $(BLD_DIR)/* $(BIN_DIR)/* $(DOC_DIR)/html $(DOC_DIR)/latex
	@echo ""
	@echo "...✓ done!"
