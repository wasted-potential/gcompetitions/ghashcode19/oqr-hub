[contributing]: #contributing
[license]: #license
[nelson]: https://github.com/nelsonmestevao
[pedro]: https://github.com/pedroribeiro22
[ricardo]: https://github.com/tymoshchuk19
[rui]: https://github.com/ruimendes29

<div align="center">
  <a href="https://hashcode.withgoogle.com">
    <img src="https://storage.googleapis.com/gweb-uniblog-publish-prod/images/hashcode_hero.max-1000x1000.png" alt="Hash Code" width="400">
  </a>
  <br>
</div>

Hash Code is a team programming competition organized by Google for students
and industry professionals across Europe, the Middle East and Africa. You pick
your team and programming language, we pick a Google engineering problem for
you to solve.

## Team

[![Nelson Estevão](https://github.com/nelsonmestevao.png?size=120)][nelson] | [![Pedro Ribeiro](https://github.com/pedroribeiro22.png?size=120)][pedro] | [![Ricardo Canela](https://github.com/tymoshchuk19.png?size=120)][ricardo] | [![Rui](https://github.com/ruimendes29.png?size=120)][rui]
:---: | :---: | :---: | :---:
[Nelson Estevão][nelson] | [Pedro Ribeiro][pedro] | [Ricardo Canela][ricardo] | [Rui Mendes][rui]
