#ifndef __SLIDES_H__
#define __SLIDES_H__
#include "photos.h"

typedef struct slides {
    int nfotos;
    int fotos[2];
} Slide;

int min(int a, int b);

int sumTags(Slide a, Photo *fotos, int nfotos, char **write);

int interestFactor(Slide a, Slide b, Photo *fotos, int nfotos);

#endif
