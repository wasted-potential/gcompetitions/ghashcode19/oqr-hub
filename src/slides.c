#include "slides.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "photos.h"

int interestFactor(Slide a, Slide b, Photo *fotos, int nfotos) {
    char **uniao = NULL;
    int s0, s1;
    char **uniao2 = NULL;
    int aV = 0, bV = 0;

    if ((a.nfotos > 1) || (b.nfotos > 1)) {
        if (a.nfotos > 1) {
            int aSizeFoto1 = fotos[a.fotos[0]].ntags;
            int aSizeFoto2 = fotos[a.fotos[1]].ntags;
            uniao = malloc(sizeof(char *) * (aSizeFoto1 + aSizeFoto2));
            s0 = sumTags(a, fotos, nfotos, uniao);
            aV = 1;
        }

        if (b.nfotos > 1) {
            int bSizeFoto1 = fotos[b.fotos[0]].ntags;
            int bSizeFoto2 = fotos[b.fotos[1]].ntags;
            uniao2 = malloc(sizeof(char *) * (bSizeFoto1 + bSizeFoto2));
            s1 = sumTags(b, fotos, nfotos, uniao2);
            bV = 1;
        }
    }

    if (a.nfotos == 1) {
        int aSizeFoto1 = fotos[a.fotos[0]].ntags;
        uniao = malloc(sizeof(char *) * aSizeFoto1);

        for (int i = 0; i < aSizeFoto1; i++) {
            uniao[i] = malloc(sizeof(char *) * 11);
            strcpy(uniao[i], fotos[a.fotos[0]].tags[i]);
        }

        s0 = aSizeFoto1;
    }

    if (b.nfotos == 1) {
        int bSizeFoto1 = fotos[b.fotos[0]].ntags;
        uniao2 = malloc(sizeof(char *) * bSizeFoto1);

        for (int i = 0; i < bSizeFoto1; i++) {
            uniao2[i] = malloc(sizeof(char *) * 11);
            strcpy(uniao2[i], fotos[b.fotos[0]].tags[i]);
        }
        s1 = bSizeFoto1;
    }
    int common = 0;

    for (int i = 0; i < s0; i++) {
        for (int j = 0; j < s1; j++) {
            if (strcmp(uniao[i], uniao2[j]) == 0) {
                common++;
            }
        }
    }
    int inAnotinB = s0 - common;
    int inBnotinA = s1 - common;
    return min(common, min(inAnotinB, inBnotinA));
}

int sumTags(Slide a, Photo *fotos, int nfotos, char **write) {
    int i, j, k = 0;
    int pi = 0;
    char *tmp;

    for (i = 0; i < fotos[a.fotos[0]].ntags; i++) {
        tmp = malloc(sizeof(char *) * 11);
        strcpy(tmp, fotos[a.fotos[0]].tags[i]);
        write[i] = tmp;
    }

    for (j = 0; j < fotos[a.fotos[1]].ntags; j++) {
        int c = 1;

        for (k = 0; k < i && c; k++) {
            if (strcmp(write[k], fotos[a.fotos[1]].tags[j]) != 0) {
                tmp = malloc(sizeof(char *) * 11);
                strcpy(tmp, fotos[a.fotos[1]].tags[j]);
                c = 0;
                pi++;
                write[i++] = tmp;
            }
        }
    }
    return pi;
}

int min(int a, int b) {
    int r = a < b ? a : b;

    return r;
}
