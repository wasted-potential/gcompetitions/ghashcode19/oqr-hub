#include "slideshow.h"
#include <stdio.h>
#include "photos.h"
#include "slides.h"

void swap(Slide *ss, int i, int j) {
    Slide temp = ss[i];

    ss[i] = ss[j];
    ss[j] = temp;
}

void printSlideShow(Slide *ss, int ns) {
    int i;

    for (i = 0; i < ns; i++) {
        printf("nf : %d , fotos : %d %d\n", ss[i].nfotos, ss[i].fotos[0],
               ss[i].fotos[1]);
    }
    printf("\n");
}

void sortSlideshow(Slide *slideshow, Photo *fotos, int ns, int nf) {
    int max = -1;
    int calc = 0;
    int k = 1;
    int i, j;

    for (i = 0; i < ns; i++) {
        k = i + 1;

        for (j = i + 1; j < ns; j++) {
            calc = interestFactor(slideshow[i], slideshow[j], fotos, nf);

            if (calc > max) {
                max = calc;
                k = j;
            }
        }
        max = -1;
        swap(slideshow, i + 1, k);
    }
}
