#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "photos.h"
#include "slides.h"
#include "slideshow.h"

int main(int argc, char **argv) {
    FILE *fp;
    int i, j, N, ntags;
    int si;  // onde estamos
    int uv;  // ultima foto vertical lida
    char *tmp;
    char ori;

    if (argc == 2) {
        fp = fopen(argv[1], "r");

        fscanf(fp, "%d\n", &N);

        Photo *photos = malloc(sizeof(struct photo) * N);
        Slide *slides = malloc(sizeof(struct slides) * N);

        /* Dá parse na estrutura */
        for (i = si = 0, uv = -1; i < N; i++) {
            fscanf(fp, "%c %d ", &ori, &ntags);

            photos[i].ori = ori;
            photos[i].ntags = ntags;

            photos[i].tags = (char **)malloc(sizeof(char *) * ntags);

            for (j = 0; j < ntags - 1; j++) {
                tmp = (char *)malloc(sizeof(char) * 11);
                fscanf(fp, "%s ", tmp);
                photos[i].tags[j] = tmp;
            }
            tmp = (char *)malloc(sizeof(char) * 11);
            fscanf(fp, "%s\n", tmp);
            photos[i].tags[j] = tmp;

            if (photos[i].ori == 'H') {
                slides[si].nfotos = 1;
                slides[si++].fotos[0] = i;
            } else if (uv == -1) {
                uv = si;
                slides[si].nfotos = 2;
                slides[si++].fotos[0] = i;
            } else {
                slides[uv].fotos[1] = i;
                uv = -1;
            }
        }

        sortSlideshow(slides, photos, si, N);

        /* Mostra a estrutura */
        /* for (i = 0; i < N; i++) { */
        /*     printf("Quantas são: %d\n", photos[i].ntags); */
        /*     printf("Orientação:%c\nNúmero de tags:%d\n", */
        /*            photos[i].ori, */
        /*            photos[i].ntags); */
        /*     printf("Tags:\n"); */

        /*     for (j = 0; j < photos[i].ntags; j++) { */
        /*         printf("%s", photos[i].tags[j]); */
        /*         printf("\n"); */
        /*     } */

        /*     printf("\n"); */

        /* } */

        printf("%d\n", si);

        for (i = 0; i < si; i++) {
            if (slides[i].nfotos == 2) {
                printf("%d %d\n", slides[i].fotos[0], slides[i].fotos[1]);
            } else {
                printf("%d\n", slides[i].fotos[0]);
            }
        }

        fclose(fp);
    }

    return 0;
}
